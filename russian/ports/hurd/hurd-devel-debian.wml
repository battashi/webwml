#use wml::debian::template title="Debian GNU/Hurd — Разработка" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="e42a3c19fa8c376678e6147f47b31ba3fc60e369" maintainer="Lev Lamberov"

<h1>
Debian GNU/Hurd</h1>
<h2>
Разработка дистрибутива</h2>

<h3>
Создание пакетов с ПО Hurd</h3>
<p>
Специфичные для Hurd пакеты сопровождаются в рамках команды <url "https://salsa.debian.org/hurd-team/">.
</p>

<h3>
Перенос пакетов Debian</h3>
<p>
Если вы хотите помочь переносу Debian GNU/Hurd, вам следует познакомиться
с системой создания пакетов Debian. После того, как вы прочтёте
доступную документацию и посетите <a
href="$(HOME)/devel/">Уголок разработчика</a>, вы будете знать, как
распаковывать пакеты Debian с исходным кодом и собирать пакеты Debian. Для
ленивых людей имеется ускоренный курс:</p>

<h3>
Получение исходного кода и сборка пакетов</h3>

<p>
Вы можете получить исходный код, просто выполнив команду <code>apt source
package</code>, эта команда также развернёт исходный код.
</p>

<p>
Для распаковки пакета Debian с исходным кодом требуется файл
<code>package_version.dsc</code> и указанные в нём файлы. Вы создаёте
каталог Debian для сборки с помощью команды
<code>dpkg-source -x package_version.dsc</code>
</p>

<p>
Сборка пакета происходит в созданном каталоге Debian для сборки
<code>package-version</code> с помощью команды
<code>dpkg-buildpackage -B "-mMyName &lt;MyEmail&gt;"</code>.
Вместо <code>-B</code> вы можете использовать
<code>-b</code>, если также хотите собрать независимые от архитектуры
части пакета (однако обычно это бесполезно, так как они уже имеются
в архиве, а для их сборки могут потребоваться дополнительные
зависимости). Вы можете добавить
<code>-uc</code>, чтобы не подписывать пакет вашим ключом pgp.</p>

<p>
Для сборки могут потребоваться дополнительные пакеты. Проще всего
выполнить команду <code>apt build-dep package</code>, которая установит все требуемые пакеты.
</p>

<p>
Удобно испольовать pbuilder. Пакет может быть собран с помощью команды
<code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/ --debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg --debootstrapopts --extra-suites=unreleased --extrapackages debian-ports-archive-keyring</code>,
затем можно использовать команду <code>pdebuild -- --binary-arch</code>, которая загрузит сборочные зависимости и др., а потом поместит результат в <code>/var/cache/pbuilder/result</code>.
</p>

<h3>
Выберите пакет</h3>
<p>
Над каким пакетом нужно поработать? Ну, над любым пакетом, который
ещё не был перенесён, но должен быть перенесён. Список таких пакетов постоянно меняется,
поэтому предпочтительно сконцентрироваться в первую очередь на пакетах с большим количеством обратных
зависимостей, список таких пакетов можно посмотреть на графике зависимости пакетов
<url "https://people.debian.org/~sthibault/graph-radial.pdf">, обновляемом каждый день,
либо в списке наиболее желаемых пакетов
<url "https://people.debian.org/~sthibault/graph-total-top.txt"> (это
список пакетов, желаемых в далёкой перспективе, список пакетов, желаемых в короткой перспективе находится здесь:
<url "https://people.debian.org/~sthibault/graph-top.txt">).
Неплохо также выбрать какой-нибудь пакет из списков устаревших пакетов
<url "https://people.debian.org/~sthibault/out_of_date2.txt"> и
<url "https://people.debian.org/~sthibault/out_of_date.txt">, поскольку обычно они
работают, но сломаны в настоящий момент лишь вероятно из-за пары каких-то причин.
Вы также можете случайным образом выбрать один из отсутствующих пакетов, либо посмотреть
журналы автоматической сборки в списке рассылки debian-hurd-build-logs, либо использовать
список пакетов, которые требуют доработки
<url "https://people.debian.org/~sthibault/failed_packages.txt"> .
</p>
<p>
Также проверьте, может быть работа по переносу какого-то пакета уже выполнена, см.
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse">,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse">,
систему отслеживания ошибок (<url "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd">) и <url "https://wiki.debian.org/Debian_GNU/Hurd">,
а также статус пакетов на buildd.debian.org, напр.
<url "https://buildd.debian.org/util-linux">.
</p>

<h4>
Пакеты, которые не будут перенесены</h4>
<p>
Некоторые из этих пакетов или их части могут быть перенесены позже, но
в настоящее время они считаются по меньшей мере непереносимыми. Обычно
они помечены как NotForUs в базе данных buildd.
</p>

<ul>
<li>
<code>base/makedev</code>, поскольку Hurd имеет свою собственную версию
этого сценария. Пакет Debian с исходным кодом содержит только версию,
специфичную для Linux.</li>
<li>
<code>base/modconf</code> и <code>base/modutils</code>, поскольку
концепция модулей специфична для Linux.</li>
<li>
<code>base/netbase</code>, поскольку оставшаяся часть этого специфична для
ядра Linux. Hurd использует
<code>inetutils</code>.</li>
<li>
<code>base/pcmcia-cs</code>, поскольку этот пакет специфичен для Linux.</li>
<li>
<code>base/setserial</code>, поскольку это специфично для ядра
Linux. Тем не менее, с переносом символьных драйверов Linux на GNU Mach, мы,
вероятно, сможем использовать эту функциональность.</li>
</ul>

<h3> <a name="porting_issues">
Общие проблемы переноса</a></h3>
<p>
<a href=https://www.gnu.org/software/hurd/hurd/porting/guidelines.html>Список
общих проблем</a> доступен на веб-сайте основной ветки разработки. Следующие общие
проблемы специфичны для Debian.</p>
<p>До того, как предпринять попытку исправить что-то, проверьте, может быть в переносе на kfreebsd*
это уже исправлено и нужно лишь распространить это решение на hurd-i386.</p>

<ul>
<li>
<code>foo : Depends: foo-data (= 1.2.3-1), но пакет не будет установлен</code>
<p>
Короткий ответ таков: пакет <code>foo</code> не был собран для hurd-i386,
это требует исправления, обратитесь к странице статуса с ошибкой сборки на
buildd.debian.org.
</p>
<p>
Обычно это случается в том случае, когда пакет <code>foo</code> не может быть в
настоящее время собран, но ранее успешно собирался. Используйте <code>apt-cache policy foo foo-data</code>,
чтобы увидеть, что, например, доступна версия <code>1.2.3-1</code> пакета
<code>foo</code>, и доступна более новая версия пакета <code>foo-data</code>, например,
<code>2.0-1</code>. Это связано с тем, что на debian-ports независящие от архитектуры (arch:all)
пакеты распространяются для всех архитектур, поэтому когда загружается более новая версия
пакета с исходным кодом <code>foo</code> (из которого собираются двоичные пакеты
<code>foo</code> и <code>foo-data</code>), устанавливается более новый arch:all
пакет <code>foo-data</code>, даже если более новый hurd-i386
двоичный пакет <code>foo</code> не может быть собран, это ведёт к несовместимости
версий. Для исправления этого требуется заставить архив debian-ports использовать dak вместо
mini-dak, работа над чем всё ещё продолжается.
</p>

</li>
<li>
<code>некоторые символы и шаблоны исчезли из файла с символами</code>
<p>
В которые пакетах имеются списки символов, которые ожидаются в
библиотеках. Однако такой список обычно создаётся в системе Linux, а потому
он включает в себя символы, которые не имеют смысла для отличных от Linux систем (например, из-за
каких-то характерных только для Linux возможностей). Можно, однако, добавить условные утверждения
в файл <code>.symbols</code>, например:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 (arch=linux-any)linuxish_function@Base 1.23
</pre></td></tr></table>

</li>
<li>
<code>Сломанная зависимость libc6</code>
<p>
Некоторые пакеты используют ошибочную зависимость от <code>libc6-dev</code>. Это
неправильно, поскольку <code>libc6</code> специфична для некоторых архитектур
GNU/Linux. Соответствующий пакет для GNU &mdash; <code>libc0.3-dev</code>,
но другие операционные системы имеют свои пакеты. Мы можем обнаружить проблему в
файле <code>debian/control</code> в дереве исходного кода. Типичное решение предполагает
обнаружение операционной системы, используя <code>dpkg-architecture</code>, и добавления
версии so-библиотеки (soname), или, что ещё лучше, использования логического ИЛИ, напр.:
<code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.
Пакет <code>libc-dev</code> является
виртуальным пакетом, которые работает для любой версии so-библиотеки, но вам следует указывать его только
как последнее средство.</p></li>
<li>
<code>undefined reference to snd_*, SND_* undeclared</code>
<p>
Некоторые пакеты используют ALSA даже на архитектурах, отличных от Linux. Пакет oss-libsalsa
предоставляет некоторую эмуляцию над OSS, но он ограничен 1.0.5, и не предоставляет
некоторые возможности, такие как все операции секвенсера.
</p>
<p>
Если пакет разрешает это, поддержка alsa должна быть отключена на всех
архитектурах <code>!linux-any</code> (например, через параметр
<code>configure</code>), к alsa <code>Build-Depends</code> добавлен спецификатор <code>[linux-any]</code>,
а к <code>Build-Conflicts</code> добавлено
обратное утверждение, такое как
<code>Build-Conflicts: libasound2-dev [!linux-any]</code>.
</p>
</li>
<li>
<code>dh_install: не удаётся найти (совпадения с) "foo" (попытка произведена в ., debian/tmp)</code>
<p>
Обычно это случается в том случае, когда код из основной ветки разработки не выполняет
установку, если не удаётся определить операционную систему. Иногда это просто глупо
(например, код не знает, что сборка разделяемой библиотеки на GNU/Hurd полностью совпадает со
сборкой на GNU/Linux) и нужно это исправить. Иногда эта проблема имеет смысл (например, не установлены
файлы служб systemd). В этом случае можно использовать dh-exec: добавьте <tt>dh-exec</tt> в список
сборочных зависимостей, выполните команду <tt>chmod +x</tt> для файла <tt>.install</tt> и добавьте к проблемным
строкам <tt>[linux-any]</tt> или <tt>[!hurd-any]</tt>.
</p>
</li>
</ul>

<h3> <a name="debian_installer">
Изучение и работа с программой установки Debian</a></h3>

<p>
Чтобы создать ISO-образ, проще всего начать с существующего образа, размещённого на <a href=hurd-cd>странице CD-образов Hurd</a>. Его можно примонтировать и скопировать:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
mount debian-sid-hurd-i386-NETINST-1.iso /mnt
cp -a /mnt /tmp/myimage
umount /mnt
chmod -R +w /tmp/myimage
</pre></td></tr></table>

<p>
Далее, можно примонтировать начальный ram-диск и, например, заменить какой-то транслятор на ту версию, которая вам нужна:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
gunzip /tmp/myimage/initrd.gz
mount /tmp/myimage/initrd /mnt
cp ~/hurd/rumpdisk/rumpdisk /mnt/hurd/
umount /mnt
gzip /tmp/myimage/initrd
</pre></td></tr></table>

<p>
Теперь можно заново собрать iso с помощью grub-mkrescue:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
rm -fr /tmp/myimage/boot/grub/i386-pc
grub-mkrescue -o /tmp/myimage.iso /tmp/myimage
</pre></td></tr></table>

</li>
</ul>
