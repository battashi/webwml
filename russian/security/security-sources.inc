#use wml::debian::translation-check translation="3121c231cfff9a41f6abfc6afdfeb2aa9435046e" maintainer="Lev Lamberov"

<ul>
<li><a href="https://security-tracker.debian.org/">Система отслеживания безопасности Debian</a>
является главным источником информации, связаной с безопасностью, и поддерживает поиск</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">Список в формате JSON</a>
  содержит описание CVE, имя пакета, номер ошибки Debian, версии пакета с исправлениями, не включает DSA
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">Список DSA</a>
  содержит DSA, включая дату, связанные номера идентификаторов CVE, версии пакета с исправлениями
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">Список DLA</a>
  содержит DLA, включая дату, связанные номера идентификаторов CVE, версии пакета с исправлениями
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
анонсы DSA</a> (рекомендации по безопасности Debian)</li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
анонсы DLA</a> (рекомендации по безопасности Debian LTS)</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa">RSS</a>
с DSA или длинная версия <a class="rss_logo" style="float: none;" href="$(HOME)/security/dsa-long">RSS</a>, включающая текст рекомендации</li>

<li> <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla">RSS</a>
с DLA или длинная версия <a class="rss_logo" style="float: none;" href="$(HOME)/lts/security/dla-long">RSS</a>, включающая текст рекомендации</li>

<li><a href="oval">Oval files</a></li>

<li>Поиск DSA (учитывается регистр)<br>
например: <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Поиск DLA (важно добавление суффикса -1)<br>
например: <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Поиск CVE<br>
например: <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>
