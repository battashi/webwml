#use wml::debian::template title="Interpretando o código de conduta do Debian" BARETITLE=true
#use wml::debian::translation-check translation="8ec27f98071e8313b1a32572c961a2417fc0f36f"

{#meta#:
<meta name="keywords" content="code of conduct, coc">
:#meta#}

<h2>Alguns comentários gerais</h2>

<p>O objetivo deste documento é fornecer algumas explicações e
exemplos de como o <a href="$(HOME)/code_of_conduct">Código de
Conduta</a> (CoC) é interpretado dentro do projeto Debian. Se
tiver alguma dúvida, entre em contato em inglês com a equipe Comunidade
(community@debian.org). Se está preocupado(a) com algo que você está pensando
em fazer e que pode violar o CoC, por favor, entre em contato com a
equipe Comunidade.</p>

<p>O objetivo do CoC é criar um espaço comunitário onde as pessoas
possam se sentir confortáveis. Isso nos ajuda a manter um coletivo de
colaboradores(as) que estejam empolgados(as) em participar do Debian e nos
ajuda a cumprir nossos objetivos de criar e manter o Debian. A comunidade Debian
é, ao mesmo tempo, um grupo de amigos(as) trabalhando em um projeto e um grupo
de colegas fazendo seu trabalho. O Debian é tanto um grande grupo social
como um projeto técnico.</p>

<p>O objetivo do CoC e dos esforços de aplicação do CoC é ajudar as pessoas
a alinharem-se aos valores compartilhados que o Debian adotou como comunidade
através do processo de resoluções gerais. Você tem que seguir e respeitar o CoC
para participar do Debian. Você não precisa ser perfeito(a) --
todo mundo comete erros ou tem um dia ruim -- o objetivo de aplicação do CoC
é ajudar as pessoas a fazerem melhor. Isso é tudo o que
pedimos: tente o seu melhor para tratar seus amigos(as) e colegas com
consideração. O CoC cobre todas as atividades dentro do Debian e aquelas que
você realiza como representante do projeto Debian. Ações punitivas,
como banimento (temporário ou permanente), ou perda de status,
também podem ocorrer se suas atividades fora do Debian impactarem o projeto ou
criarem um espaço inseguro ou prejudicial no Debian.</p>

<p>Este é um <strong>documento vivo</strong>. Ele mudará ao longo do
tempo conforme o Debian evolui, mudando o que é considerado normal e ideal tanto
dentro como fora do projeto. O código de conduta Debian foi inicialmente
ratificado em 2014. Não mudou desde então, embora tenham mudado as expectativas
e melhores práticas nas comunidades de software livre e de código aberto, e em
tecnologia em geral.</p>

<h2>1. Seja respeitoso(a)</h2>

<p><strong>Em um projeto do tamanho do Debian, inevitavelmente haverá
pessoas com quem você possa discordar ou achar difícil
colaborar. Aceite isso, mas mesmo assim, mantenha-se respeitoso(a). Desacordo
com ações ou opiniões alheias não é desculpa para mau comportamento ou
ataques pessoais. Uma comunidade na qual as pessoas sintam-se ameaçadas não é
uma comunidade saudável.</strong></p>

<p>Todos(as) os(as) participantes da comunidade e o mundo em geral merecem
respeito. Respeito não é algo a ser conquistado no Debian, é
algo que cada participante da comunidade merece, independentemente
de sua idade, gênero, tamanho corporal, educação, etnia ou outros
fatores.</p>

<p>Debian é uma comunidade aberta. Todas as culturas e crenças são bem-vindas
e reconhecidas desde que não prejudiquem as outras. Suas próprias
expectativas ou sua própria formação cultural não são desculpas para
violar o CoC ou ser desrespeitoso(a) com outra pessoa dentro do
comunidade Debian ou dentro de sua função como participante da comunidade
Debian. O Debian tem sua própria cultura. Ao trabalhar com contribuidores(as)
ou usuários(as) Debian, por favor, respeite as normas do Debian e represente o
Debian positivamente.</p>

<p>As pessoas no Debian vêm de diferentes origens culturais, têm
experiências diferentes, e podem não ser fluentes ou não sentirem-se
confortáveis no idioma de uma determinada discussão. Isso significa que é
importante assumir boa-fé (veja abaixo) e compreender
diferenças nos estilos de comunicação. Isso não significa, contudo, que
é aceitável comunicar intencionalmente de forma inadequada ou não
mudar seu estilo de comunicação para atender às normas da comunidade, uma vez
que esta questão seja levantada.</p>

<h3>Exemplos de comportamento desrespeitoso</h3>

<p>O que segue é uma lista de exemplos <strong>não exaustivos</strong>
de comportamentos desrespeitosos:</p>

<ul>
  <li>Profanação dirigida a uma pessoa ou a seu trabalho</li>
  <li>Insultar alguém por sua idade, deficiência, gênero, sexualidade,
      tamanho corporal, religião, nacionalidade, raça, etnia, casta, tribo,
      educação, tipos de contribuição ou status dentro do Debian e/ou
      software livre e de código aberto, usando um dos itens acima como um
      insulto ou fazendo declarações depreciativas sobre um grupo</li>
  <li>Usar intencionalmente pronomes ou nomes errados
      (por exemplo, <q>deadnaming</q>) de uma pessoa</li>
  <li>Contatar agressivamente ou repetidamente alguém após ser solicitado(a)
      a parar</li>
  <li>Não fazer esforços de boa-fé para chegar a um acordo com pessoas ou
      para mudar comportamentos que são contrários aos valores do Debian</li>
</ul>

<h2>2. Considere boa-fé</h2>

<p><strong>Contribuidores(as) Debian têm muitas maneiras de alcançar nosso
objetivo comum de um sistema operacional livre: as maneiras de outra pessoa
fazer algo podem diferir dos seus caminhos. Suponha que outras pessoas estejam
trabalhando colaborativamente para este objetivo. Observe que muitos(as) de
nossos(as) colaboradores(as) não são falantes nativos de inglês ou podem ter
diferentes formações culturais.</strong></p>

<p><strong>O Debian é um projeto global.</strong> O Debian inclui pessoas
de diferentes origens, experiências, estilos de comunicação
e normas culturais. Como tal, é particularmente importante assumir
boa-fé. Isso significa supor, como é razoável, que a pessoa
com quem você está falando não está tentando machucá-lo(a) ou insultá-lo(a).</p>

<p>Para assumirmos boa-fé, também devemos agir de boa-fé. Isto
também significa que você deve assumir que alguém está tentando o seu melhor, e
que você não deve machucá-lo(a) ou insultá-lo(a). Não é aceitável importunar
alguém intencionalmente dentro do Debian.</p>

<p>Assumir boa-fé inclui comunicação, comportamento e
contribuição. Isso significa assumir que quem contribui, independentemente
de suas contribuições, está fazendo o esforço de que é capaz e
fazendo isso com integridade.</p>

<h3>Exemplos de comportamentos que não consideram boa-fé</h3>

<p>Novamente, a lista seguinte <strong>não é exaustiva</strong>:</p>

<ul>
  <li>"Trolagem"</li>
  <li>Assumir que alguém está sendo rude (trolando)</li>
  <li>Usar frases como <q>Eu sei que você não é estúpido(a)</q> ou <q>Você
      não poderia ter feito isso intencionalmente, então você deve ser
      burro(a)</q></li>
  <li>Insultar as contribuições de alguém</li>
  <li>Promover antagonismos - <q>mexer com sentimentos
      alheios</q> para produzir efeitos de inimizade</li>
</ul>

<h2>3. Seja colaborativo(a)</h2>

<p><strong>O Debian é um projeto grande e complexo; há sempre mais
para aprender dentro do Debian. É bom pedir ajuda quando você precisar.
Da mesma forma, as ofertas de ajuda devem ser vistas no contexto de nosso
objetivo compartilhado de melhorar o Debian.</strong></p>

<p><strong>Quando você faz algo para o benefício do projeto, esteja
disposto(a) a explicar aos(às) outros(as) como funciona, para que possam
construir a partir do seu trabalho e assim torná-lo ainda melhor.</strong></p>

<p>Nossas contribuições ajudam outros(as) colaboradores(as), o projeto e
nossos(as) usuários(as). Trabalhamos abertamente sob o ethos de que qualquer
pessoa que queira contribuir deva ser capaz de fazê-lo, dentro do razoável. Todo
mundo no Debian tem diferentes origens e diferentes habilidades. Isso significa
que você deve ter atitudes positivas e construtivas e, sempre que possível, que
você deve fornecer assistência, aconselhamento ou orientação. Valorizamos o
consenso, embora haja momentos em que decisões democráticas serão tomadas ou que
a direção será decidida por aquelas pessoas que estão dispostas e aptas a
empreender uma atividade.</p>

<p>Equipes diferentes usam ferramentas diferentes e têm normas diferentes de
colaboração. Isso pode significar coisas como reuniões síncronas
semanais, notas compartilhadas ou processos de revisão de código. Porque
as coisas são feitas de uma certa maneira não significa que é a melhor ou a
única, e as equipes devem estar abertas a discutir novos métodos de
colaboração.</p>

<p>Parte de ser uma pessoa colaborativa também é uma suposição de boa fé
(veja acima) de que outras pessoas também estão colaborando em vez de
presumir que estão <q>perseguindo</q> ou ignorando você.</p>

<p>Boa colaboração é mais valiosa do que habilidades técnicas. Ser
um(a) bom(boa) colaborador(a) técnico(a) não torna aceitável ser um(a)
participante prejudicial da comunidade.</p>

<h3>Exemplos de má colaboração</h3>

<ul>
   <li>Recusar-se a adotar as normas de contribuição de uma equipe</li>
   <li>Insultar outros(as) colaboradores(as)/colegas</li>
   <li>Recusar-se a trabalhar com outras pessoas, a menos que isso represente
     uma ameaça a sua segurança ou bem-estar</li>
</ul>

<h2>4. Tente ser conciso(a)</h2>

<p><strong>Lembre-se de que o que você escreve uma vez será lido por
centenas de pessoas. Escrever um e-mail curto significa que as pessoas poderão
entender a conversa da forma mais eficientemente possível. Quando uma longa
explicação for necessária, considere adicionar um resumo.</strong></p>

<p><strong>Tente trazer novos argumentos para uma conversa para que cada
e-mail adicione algo único ao tópico. Tenha em mente que o
o restante do encadeamento de mensagens (thread) ainda contém as outras
mensagens com o argumentos que já foram feitos.</strong></p>

<p><strong>Tente manter o foco, especialmente em discussões que já são
muito grandes.</strong></p>

<p>Alguns tópicos não são apropriados para o Debian, incluindo alguns
temas contenciosos de natureza política ou religiosa. O Debian é um
ambiente de colegas tanto quanto de amigos(as). Trocas de ideias coletivas e
públicas devem ser respeitosas, sobre o tema em questão e
profissionais. Usar uma linguagem concisa e acessível é importante,
especialmente porque muitos(as) contribuidores(as) do Debian não são falantes
nativos(as) de inglês e grande parte das comunicações do projeto são em inglês.
É importante ser claro(a) e explícito(a) e, quando possível, explicar ou
evitar expressões idiomáticas (nota: usar expressões idiomáticas, por exemplo,
não é uma violação do código de conduta. Evitá-las é apenas uma boa prática
geral para maior clareza).</p>

<p>Nem sempre é fácil transmitir significado e intenção por meio de texto ou
entre culturas. Ter a mente aberta, assumir boas intenções e
se esforçar são as coisas mais importantes em uma conversa.</p>

<h2>5. Seja aberto(a)</h2>

<p><strong>A maioria das formas de comunicação usadas no Debian permitem
comunicação pública e privada. De acordo com o parágrafo três do contrato
social, deve-se utilizar preferencialmente meios públicos de comunicação
para mensagens relacionadas ao Debian, a menos que seje algo
sensível.</strong></p>

<p><strong>Isso também se aplica a mensagens de ajuda ou relacionadas ao suporte
do Debian; não só é muito mais provável que um pedido de apoio público
resulte em uma resposta à sua pergunta, como também garante-se que qualquer
erro cometido inadvertidamente por pessoas que responderam à sua pergunta seja
mais facilmente detectado e corrigido.</strong></p>

<p>É importante manter o maior número possível de comunicações públicas quanto
possível. Muitas listas de discussão do Debian podem ser acessadas por qualquer
pessoa ou ter arquivos acessíveis ao público. Os arquivos podem ser gravados e
armazenados por fontes não Debian (por exemplo, o Internet Archive). Deve-se
assumir que o que foi dito em uma lista de discussão do Debian
é <q>permanente</q>. Muitas pessoas também mantêm registros de conversas de
IRC.</p>

<h3>Privacidade</h3>

<p>As conversas privadas no contexto do projeto ainda são
consideradas como estando ao abrigo do código de conduta. Incentivamos que se
relate o que foi dito em uma conversa privada quando for inapropriado ou
inseguro (veja acima para exemplos).</p>

<p>Ao mesmo tempo, é importante respeitar as conversas privadas,
que não devem ser compartilhadas por questões de segurança. Certo
lugares, como a lista de discussão debian-private, se enquadram nesta
categoria.</p>

<h2>6. Em caso de problemas</h2>

<p><strong>Embora este código de conduta deva ser seguido pelos(as)
participantes, reconhecemos que às vezes as pessoas podem ter um dia ruim,
ou desconhecer algumas das diretrizes deste código de conduta. Quando
isso acontecer, você pode responder a essas pessoas e indicar este código de
conduta. Tais mensagens podem ser públicas ou privadas, o que for
mais apropriado. No entanto, independentemente de a mensagem ser pública
ou não, ela ainda deve aderir às partes relevantes deste código de
conduta; em particular, não deve ser abusiva ou
desrespeitosa. Assuma boa-fé; é mais provável que os(as) participantes
desconheçam seu mau comportamento do que estejam tentando intencionalmente
degradar a qualidade da discussão.</strong></p>

<p><strong>Infratores(as) graves ou persistentes serão temporariamente ou
permanentemente banidos(as) de se comunicar através dos sistemas Debian.
As reclamações devem ser feitas (em particular) aos(às) administradores(as)
do fórum de comunicação Debian em questão. Para encontrar as informações de
contato para esses(as) administradores(as), por favor veja a página da
estrutura organizacional do Debian.</strong></p>

<p>O objetivo do código de conduta é fornecer orientação para as
pessoas sobre como manter o Debian como uma comunidade acolhedora. As pessoas
devem se sentir à vontade para participar como são e não como as outras pessoas
esperam que sejam. O código de conduta do Debian descreve coisas que as pessoas
devem fazer, e não maneiras que não devam se comportar. Este documento
fornece informações sobre como a equipe Comunidade (Community) interpreta o
código de conduta. Diferentes participantes da comunidade Debian podem
interpretar esse documento de forma diferente. O mais importante é que as
pessoas se sintam confortáveis, seguras e bem-vindas dentro do Debian.
Independentemente de como os termos são chamados neste documento, se alguém não
se sentir confortável, seguro(a) e/ou bem-vindo(a), deve entrar em contato
(em inglês) com a
<a href="https://wiki.debian.org/Teams/Community">equipe Comunidade</a>.</p>

<p>Se alguém estiver preocupado(a) com a possibilidade de ter feito algo
inapropriado ou está pensando em fazer algo que acha que pode ser
inapropriado, também é incentivado(a) a entrar em contato com a equipe
comunidade.</p>

<p>Como o Debian é tão grande, a equipe Comunidade não pode e não
monitora proativamente todas as comunicações, embora às vezes os(as)
participantes possam vê-las de passagem. Como tal, é importante que a comunidade
Debian trabalhe com a equipe Comunidade.</p>

<h2>Falhando ao seguir o código de conduta</h2>

<p>Ninguém deve ser perfeito(a) o tempo todo. Errar
não é o fim do mundo, embora possa resultar em alguém
cobrando por melhorias. Dentro do Debian há uma expectativa de
esforços de boa-fé para fazer o bem e fazer melhor. Violações repetidas do CoC
podem resultar em represálias ou restrições na interação com a comunidade,
incluindo, mas não se limitando, a:</p>

<ul>
   <li>advertências oficiais;</li>
   <li>banimentos temporários ou permanentes de listas de e-mail, canais de IRC
   ou outros meios de comunicação</li>
   <li>remoção temporária ou permanente de direitos e privilégios; ou</li>
   <li>rebaixamento temporário ou permanente de status dentro do projeto
   Debian.</li>
</ul>
