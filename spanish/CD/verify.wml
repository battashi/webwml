#use wml::debian::cdimage title="Verificar la autenticidad de las imágenes de Debian" BARETITLE=true
#use wml::debian::translation-check translation="e96ee42901d20a8878ded2c462204cd1b7a7210f" maintainer="alexlikerock" 

<p>
Las publicaciones oficiales de las imágenes de instalación de Debian vienen con archivos de 
comprobación firmados; encuéntrelos junto con las imágenes en los directorios
<code>iso-cd</code>, <code>jigdo-dvd</code>, <code>iso-hybrid</code> etc.
Estos archivos permiten revisar que las imágenes que se descargan son correctas.
Lo primero de todo, la suma de  comprobación puede usarse para revisar que 
las imágenes no hayan sido dañadas durante la descarga.
En segundo lugar, las firmas en los ficheros de suma de comprobación 
permiten confirmar que las imágenes son las creadas y publicadas  
por Debian, y que no han sido manipuladas.
</p>

<p>
Para validar el contenido de in archivo de imagen, asegúrese de usar la 
herramienta apropiada para sumas de verificación.

Para cada versión publicada existen archivos de suma de comprobación
con algoritmos fuertes (SHA256 y SHA512); debería usar las herramientas correspondientes
<code>sha256sum</code> o <code>sha512sum</code> para trabajar con ellos.
</p>

<p>
Para asegurarse de que los propios ficheros de sumas de verificación están correctos, use GnuPG para
verificarlos contra los ficheros de firma que se acompañan (p.ej.: <code>SHA512SUMS.sign</code>).
Las claves usadas para esas firmas están todas en el 
<a href="https://keyring.debian.org">Llavero GPG de Debian</a> y la mejor manera para verificarlas 
es usando el llavero para validar via la red de confianza.
Para hacer la vida más fácil para las personas que no tienen acceso a una máquina Debian, aquí están los detalles de las claves 
que se han usado para las publicaciones en años recientes, y enlaces para descarga directa de las las claves públicas:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
