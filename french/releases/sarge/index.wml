#use wml::debian::template title="Informations sur la version « Sarge » de Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c828123435e049f9c2b89a8e80a66d0817b40439" maintainer="Jean-Paul Guillonneau"

<p>Debian GNU/Linux 3.1 (<em>Sarge</em>) a été publiée le
6 juin 2005.
Cette version comprend de nombreuses modifications décrites dans notre
<a href="$(HOME)/News/2005/20050606">communiqué de presse</a> et les
<a href="releasenotes">notes de publication</a>.</p>

<p><strong>Debian GNU/Linux 3.1 a été remplacée par
<a href="../etch/">Debian GNU/Linux 4.0 (<q>Etch</q>)</a>.
La prise en charge des mises à jour de sécurité a cessé depuis la fin mars 2008.
</strong></p>

<p>Pour obtenir et installer Debian GNU/Linux 3.1, veuillez vous reporter à la
page des informations d'installation et au guide d'installation. Pour mettre à
niveau à partir d'une ancienne version de Debian, veuillez vous reporter aux
instructions des <a href="releasenotes">notes de publication</a>.</p>

<p>les architectures suivantes étaient prises en charge dans cette publication :</p>

<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">PC 32 bits (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/m68k/">Motorola 680x0</a>
<li><a href="../../ports/mips/">MIPS (gros-boutiste)</a>
<li><a href="../../ports/mipsel/">MIPS (petit-boutiste)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Contrairement à nos souhaits, certains problèmes pourraient toujours exister
dans cette version, même si elle est déclarée <em>stable</em>. Nous avons
réalisé <a href="errata">une liste des principaux problèmes connus</a>.
</p>
